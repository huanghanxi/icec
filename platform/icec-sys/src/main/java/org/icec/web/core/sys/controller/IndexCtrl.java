package org.icec.web.core.sys.controller;

import java.util.List;

import org.icec.common.base.tips.Tip;
import org.icec.common.exception.IcecException;
import org.icec.common.model.TreeModel;
import org.icec.common.utils.TreeBuild;
import org.icec.common.web.BaseController;
import org.icec.web.core.shiro.annotation.CurrentUser;
import org.icec.web.core.sys.model.SysGlobal;
import org.icec.web.core.sys.model.SysMenu;
import org.icec.web.core.sys.model.SysUser;
import org.icec.web.core.sys.service.SysGlobalService;
import org.icec.web.core.sys.service.SysMenuService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
public class IndexCtrl extends BaseController{
	@Autowired
	private SysGlobalService sysGlobalService;
	@Autowired
	private SysMenuService sysMenuService;

	@GetMapping({ "/", "/index" })
	public String index(@CurrentUser SysUser user, ModelMap model) {
		model.addAttribute("user", user);
		List<SysMenu> menuList = sysMenuService.findMenuByUserId(user.getId());
		List<TreeModel> tree = TreeBuild.buildTree(menuList);
		model.addAttribute("menuList", tree);
		SysGlobal global = sysGlobalService.getGlobal();
		model.addAttribute("global", global);
		return "sys/index";
	}
	@GetMapping({  "/index2" })
	public String index2(@CurrentUser SysUser user, ModelMap model) {
		model.addAttribute("user", user);
		List<SysMenu> menuList = sysMenuService.findMenuByUserId(user.getId());
		List<TreeModel> tree = TreeBuild.buildTree(menuList);
		model.addAttribute("menuList", tree);
		SysGlobal global = sysGlobalService.getGlobal();
		model.addAttribute("global", global);
		return "sys/index2";
	}
	@GetMapping({ "/home" })
	public String home(@CurrentUser SysUser user, ModelMap model) {
		model.addAttribute("user", user);
		List<SysMenu> menuList= sysMenuService.findHomeMenu();
		String homeUrl="/main";
		if(menuList.size()>0) {
			SysMenu menu=menuList.get(0);
			homeUrl=menu.getHref();
		}
		return "redirect:"+homeUrl;
	}

	@RequestMapping({ "/main" })
	public String main() {
		return "sys/home";
	}
	
	@RequestMapping({ "/error2" })
	public String error() {
		throw new IcecException(500, "Sam 错误");
	}
	/**
	 * 保持session不过期
	 * @return
	 */
	@RequestMapping({"/refreshSession"})
	public @ResponseBody Tip refreshSession() {
		return SUCC;
	}
}
