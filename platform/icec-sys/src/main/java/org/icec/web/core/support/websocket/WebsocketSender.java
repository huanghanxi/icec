package org.icec.web.core.support.websocket;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.messaging.simp.SimpMessageSendingOperations;
import org.springframework.stereotype.Service;

@Service
public class WebsocketSender {
	@Autowired(required=false)
	private SimpMessageSendingOperations template;
	/**
	 * 向topic发送消息
	 * @param topic
	 * @param text
	 */
	public void send(String topic,Object text) {
		template.convertAndSend(topic, text);
	}
	/**
	 * 向指定用户发送消息
	 * @param user
	 * @param destination
	 * @param payload
	 */
	public void send2user(String user,String destination,Object payload) {
		template.convertAndSendToUser(user, destination, payload);
	}
}
