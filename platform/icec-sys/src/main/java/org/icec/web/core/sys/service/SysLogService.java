package org.icec.web.core.sys.service;
import org.springframework.stereotype.Service;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;
import org.beetl.sql.core.engine.PageQuery;
import org.icec.web.core.sys.dao.SysLogDao;
import org.icec.web.core.sys.model.SysLog;

/*
* 
* gen by icec  2017-11-01
*/
@Service
public class SysLogService   {
	@Autowired
	private SysLogDao  sysLogDao ;
	
	/**
	*
	*保存
	*/
	@Transactional
	public void save(SysLog sysLog){
		sysLogDao.insert(sysLog);
	}
	/**
	 * 日志分页查询
	 * @param query
	 * @return
	 */
	public PageQuery<SysLog> queryUser(PageQuery<SysLog> query) {
		return sysLogDao.pageQuery(query);
	}
}
