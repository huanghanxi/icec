package org.icec.web.core.sys.globalcustom;

import java.util.Map;

public interface GlobalConfigurable {
	public String getPrefix();

	public Map<String, String> getCustoms();
	public void setCustoms(Map<String, String> customs);
}
