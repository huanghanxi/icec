pageQuery
===
* 分页查询
 
	select
	@pageTag(){
	#use("cols")#  
	@} 
	from  sys_global_custom
	where #use("condition")#

sample
===
* 注释

	select #use("cols")# from sys_global_custom where #use("condition")#

cols
===

	id,f_global_id,f_key,f_value

updateSample
===

	id=#id#,f_global_id=#fGlobalId#,f_key=#fKey#,f_value=#fValue#

condition
===

	 1 = 1  
@if(!isEmpty(fGlobalId)){
 and f_global_id=#fGlobalId#
@}
@if(!isEmpty(fKey)){
 and f_key=#fKey#
@}
@if(!isEmpty(fValue)){
 and f_value=#fValue#
@}
