package org.icec.gen;

import java.io.IOException;
import java.util.Properties;

import org.beetl.sql.core.ClasspathLoader;
import org.beetl.sql.core.ConnectionSource;
import org.beetl.sql.core.ConnectionSourceHelper;
import org.beetl.sql.core.Interceptor;
import org.beetl.sql.core.NameConversion;
import org.beetl.sql.core.SQLLoader;
import org.beetl.sql.core.SQLManager;
import org.beetl.sql.core.UnderlinedNameConversion;
import org.beetl.sql.core.db.AbstractDBStyle;
import org.beetl.sql.core.db.MySqlStyle;
import org.beetl.sql.ext.DebugInterceptor;
import org.icec.gen.core.AutoGen;
import org.icec.gen.core.GenConfig;

public class GenBqlTools {
		
	
	public static void main(String[] args) throws IOException {
		Properties pro=new Properties();
		pro.load(GenBqlTools.class.getResourceAsStream("/jdbc.properties"));
		ConnectionSource source = ConnectionSourceHelper.getSimple(
				(String)pro.get("datasource.driver"),
				(String)pro.get("datasource.url"),
				(String)pro.get("datasource.username"),
				(String)pro.get("datasource.password"));
		// 采用mysql习俗
		AbstractDBStyle style = new MySqlStyle();
		// sql语句放在classpagth的/sql 目录下
		SQLLoader loader = new ClasspathLoader("/sql");
		NameConversion nc = new  UnderlinedNameConversion();
		SQLManager sqlManager = new SQLManager(style,loader,source,nc,new Interceptor[]{new DebugInterceptor()});
		GenConfig config = new GenConfig();
		config.setIfHtml(true);//生成html,不需要生成html时，设置false
		String pkg="org.icec.web.core.sys";//包的最后一位是模块名称
		String table="test_stu";//需要生成的表
		String project=".";//生成到哪个项目中
		AutoGen.go(config,sqlManager, project, pkg, table);
	}


	
}
