#jasperreport 说明
	The JasperReports Library is the world's most popular open source reporting engine. It is entirely written in Java and it is able to use data coming from any kind of data source and produce pixel-perfect documents that can be viewed, printed or exported in a variety of document formats including HTML, PDF, Excel, OpenOffice and Word.
	重点jasperreport 是java语言编写的开源的报表引擎，可以用于展示、打印、导出多种文档格式，如html,pdf,excel,word等。
	（jasperreport 原名ireport  ，ireport已经不在维护。）
	
	
# icec-report使用说明
	icec 本身就是maven管理的多模块系统；icec-report是新增的用来集成jaserreport的模块，如果需要使用他的功能，那么就在你的主模块中引入maven依赖；如果不需要，不依赖该模块即可，不影响最终发布包。
	<dependency>
			<groupId>org.icec</groupId>
			<artifactId>icec-ireport</artifactId>
			<version>${icec.version}</version>
		</dependency>

# 如何使用？
	icec-ireport 里面已经给出显示和导出pdf的案例，对应代码
	/org/icec/web/ireport/controller/IreportController.java
	查看演示方式：http://localhost:8080/ireport/showpdf   （前提是已经将模块集成到项目中）
 
# 一些问题？

	icec-ireport项目的maven依赖中，有个包会找不到，对，他就是
     <dependency>
			<groupId>org.icec.jasperfont</groupId>
			<artifactId>msyh</artifactId>
			<version>1.0</version>
		</dependency>
	
	这个包，是我自己用jaserreport studio 打的字体包（微软雅黑），不在maven仓库，所以报错了。
	需要手动下载jar包，放到本机仓库中，会用maven的都懂。
	msyh-1.0.jar文件所在路径icec-ireport/lib/msyh-1.0.jar_改名

# jasperreport 中文显示问题
	通过上一步设置，在jasperreport模板中，字体设置成msyh的话，中文就可以正常显示了。如果想使用其他字体，那么就要参考官网打包方法，生成新的jar，参考我的办法，引入到项目中，就可以了。
	官网关于自定义字体的url:
	https://community.jaspersoft.com/wiki/custom-font-font-extension		

# jasperreport 模板设计工具
	
	推荐使用	jasperreport studio （ireport 已过时）
	下载地址：
	https://community.jaspersoft.com/project/jaspersoft-studio
	
	
			