package org.icec.sms.controller;

import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.beetl.sql.core.engine.PageQuery;
import org.icec.common.base.tips.Tip;
import org.icec.common.web.BaseController;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.icec.sms.model.SmsRecord;
import org.icec.sms.model.SmsTemplate;
import org.icec.sms.service.SmsRecordService;
import org.icec.sms.service.SmsTemplateService;
import org.icec.web.core.shiro.annotation.CurrentUser;
import org.icec.web.core.sys.model.SysUser;

/*
* 
* gen by icec  2018-10-29
*/
@Controller
@RequestMapping("sms/smsRecord")
public class SmsRecordCtrl   extends BaseController{
	@Autowired
	private SmsRecordService  smsRecordService ;
	@Autowired
	private SmsTemplateService smsTemplateService;
	@RequestMapping("add")
	public String add(Model model) {
		return "sms/add";
	}
	/**
	*
	*保存
	*/
	@RequestMapping("save")
	@ResponseBody
	public Tip save(SmsRecord smsRecord,   @CurrentUser SysUser optuser ){
		smsRecordService.save(smsRecord, optuser);
		return SUCC;
	}
	/**
	 * 进入查询界面
	 * 
	 * @return
	 */
	@RequestMapping("list")
	public String listInit(ModelMap model) {
		List<SmsTemplate> list = smsTemplateService.getAll();
		model.addAttribute("tempList", list);
		return "sms/recordList";
	}
	
	/**
	 * 进入修改界面
	 * 
	 * @param id
	 * @param model
	 * @return
	 */
	@RequestMapping("edit/{id}")
	public String edit(@PathVariable Integer id, ModelMap model) {
		SmsRecord smsRecord=smsRecordService.get(id);
		model.addAttribute("smsRecord", smsRecord);
		return "sms/edit";
	}
	/**
	 * 更新数据逻辑
	 * 
	 * @param SmsRecord
	 * @return
	 */
	@RequestMapping("update")
	@ResponseBody
	public Tip update(SmsRecord smsRecord ,@CurrentUser SysUser optuser ) {
		smsRecordService.update(smsRecord,optuser);
		return SUCC;
	}
	/**
	 * 按主键删除
	 * 
	 * @param ids
	 *@return
	 */
	@RequestMapping("deleteAll")
	@ResponseBody
	public Tip deleteAll(String ids, @CurrentUser SysUser optuser) {
		if (ids == null) {
			return SUCC;
		}
		smsRecordService.deleteAll(ids, optuser);
		return SUCC;
	}
	/**
	 * 一键删除未发送短信
	 * @return
	 */
	@RequestMapping("deleteSms")
	@ResponseBody
	public Tip deleteSmsWithNoSend() {
		smsRecordService.deleteSmsWithNoSend();
		return SUCC;
	}
	/**
	 * 查询逻辑
	 * 
	 * @param pageNumber
	 * @param pageSize
	 * @param user
	 * @return
	 */
	@RequestMapping("query")
	@ResponseBody
	public PageQuery<SmsRecord> query(@RequestParam(defaultValue = "1") Integer pageNumber, Integer pageSize,
			SmsRecord smsRecord) {
		PageQuery<SmsRecord> query = new PageQuery<SmsRecord>();
		query.setPageNumber(pageNumber);
		if(pageSize!=null) {
			query.setPageSize(pageSize);
		}
		smsRecord.setDelFlag(SmsRecord.DEL_FLAG_NORMAL);
		if(StringUtils.isEmpty(smsRecord.getPhone())) {
			smsRecord.setPhone(null);
		}
		if(StringUtils.isEmpty(smsRecord.getTemplateCode())) {
			smsRecord.setTemplateCode(null);
		}
		query.setParas(smsRecord);
		query = smsRecordService.pageQuery(query);
		return query;
	}
}
