#!/bin/bassh
APP_NAME=ICECAdminMain

tpid=`ps -ef|grep $APP_NAME|grep -v grep|grep -v kill|awk '{print $2}'`
if [ ${tpid} ]; then
        echo $APP_NAME' is running.'
else
        echo $APP_NAME' is NOT running.'
fi
